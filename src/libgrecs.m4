# libgrecs.m4 serial 1
dnl This file is part of Grecs
dnl Copyright (C) 2007-2022 Sergey Poznyakoff
dnl
dnl Grecs is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 3, or (at your option)
dnl any later version.
dnl
dnl Grecs is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with Grecs.  If not, see <http://www.gnu.org/licenses/>.

dnl AM_LIBGRECS([minversion],[if-found],[if-notfound])
AC_DEFUN([AM_LIBGRECS],[
 cv_libgrecs=no
 save_LIBS=$LIBS
 AC_CHECK_LIB([grecs],[grecs_parse],
   [AC_CHECK_HEADER([grecs.h], [cv_libgrecs=yes])])
 if test "$cv_libgrecs" = "yes"; then
   LIBS="$save_LIBS -lgrecs"
   m4_if($1,,,[
     AC_RUN_IFELSE([AC_LANG_PROGRAM([[#include <stdlib.h>
       #include <grecs.h>
       ]],
       [exit(grecs_version_ok("[libgrecs ]$1"));])],
       [],
       [cv_libgrecs=no])])
   if test "$cv_libgrecs" = "yes"; then
     m4_if($2,,[
       AC_DEFINE([HAVE_GRECS_H],[1],
                 [Define if <grecs.h> is available])
       AC_DEFINE([HAVE_LIBGRECS],[1],
                 [Define if libgrecs is available])
       save_LIBS=$LIBS
      ],[
       $2])
    else
     m4_if($3,,:,[$3])
    fi
    LIBS=$save_LIBS
  fi
  ])
