# This file is part of grecs  -*- Autotest -*-
# Copyright (C) 2021-2022 Sergey Poznyakoff
#
# Grecs is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Grecs is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Grecs.  If not, see <http://www.gnu.org/licenses/>.
AT_SETUP([Internal preprocessor])

AT_CHECK([
AT_DATA([input],
[enim a;
  ;/*
  locutorum verbum
  */ vocabulum
cras b;
])
gcfpp input
],
[0],
[#line 1 "input"
enim a;
  ;
#line 4 "input"  
 vocabulum
cras b;
])
 
AT_CHECK([
AT_DATA([input],
[begin
// Inline comment
# Another online comment
statement
/* Multiline
   comments
   span several
   lines */
end
inline /* 1 */ C/* 2 */style /* text */comments
])
gcfpp input
],
[0],
[#line 1 "input"
begin
#line 4 "input"
statement

#line 8 "input"

end
inline  Cstyle comments
],
[ignore])

AT_CHECK([
AT_DATA([input],
[/* Initial
 comment
*/
info "Quorum Aenean aliquam rhoncus leo,\
 vel elementum ex imperdiet\
 vitae." // Comment 1
 "Proin maximus, nunc interdum" # Comment 2 
 "varius condimentum";
text <<EOF
Ut enim ad minim
veniam, quis
adipisci
commodo consequat
eo finem
EOF;/* Comment 
 3
*/
number 10;
])
gcfpp input
],
[0],
[#line 3 "input"

info "Quorum Aenean aliquam rhoncus leo,\
 vel elementum ex imperdiet\
 vitae."  "Proin maximus, nunc interdum"  "varius condimentum";
#line 9 "input"
text <<EOF
Ut enim ad minim
veniam, quis
adipisci
commodo consequat
eo finem
EOF;
#line 17 "input"

number 10;
])

AT_CHECK([
AT_DATA([input],
[first
#line 20 "i.cf"
/*
-
*/
line
])
gcfpp input
],
[0],
[#line 1 "input"
first
#line 20 "i.cf"

#line 22 "i.cf"

line
])

AT_CHECK([
AT_DATA([input],
[#abend "Failed"
tez
])
gcfpp input
],
[0],
[],
[input:1.1-16: Failed
])

#
#
#
AT_CHECK([
AT_DATA([input],
[Ut enim ad minim
#include "a"
veniam, quis
adipisci
#include "b"
commodo consequat
eo finem
])
AT_DATA([a],[exercitation
ullamco
])
AT_DATA([b],[labori nisi
ut aliquip ex
ea
])
gcfpp input
],
[0],
[#line 1 "input"
Ut enim ad minim
#line 1 "./a"
exercitation
ullamco
#line 3 "input"
veniam, quis
adipisci
#line 1 "./b"
labori nisi
ut aliquip ex
ea
#line 6 "input"
commodo consequat
eo finem
])

#
#
#
AT_CHECK([
AT_DATA([a.in],
[Duis aute irure
#include_once "a.d/*.in"
est laborum.
])

mkdir a.d
AT_DATA([a.d/b.in],
[dolor in
# reprehenderit
# in
voluptate
])

AT_DATA([a.d/c.in],
[velit esse
cupidatat non proident,
])

AT_DATA([a.d/d.in],
[sunt in culpa
#warning "values of B give rise to dom"
qui officia
deserunt
])

gcfpp a.in
],
[0],
[#line 1 "a.in"
Duis aute irure
#line 1 "a.d/b.in"
dolor in
#line 4 "a.d/b.in"
voluptate
#line 1 "a.d/c.in"
velit esse
cupidatat non proident,
#line 1 "a.d/d.in"
sunt in culpa
qui officia
deserunt
#line 3 "a.in"
est laborum.
],
[a.d/d.in:2.1-40: warning: values of B give rise to dom
])

AT_CLEANUP
